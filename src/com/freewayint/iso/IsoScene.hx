package com.freewayint.iso;

import com.freewayint.elements.FirePlace;
import com.freewayint.elements.Grid;
import com.freewayint.units.Unit;
import com.freewayint.units.UnitDragEvent;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.DisplayObject;
import openfl.display.Sprite;

/**
 * ...
 * @author cjay
 */

 // Данный класс в первую очередь является контейнером для изометрических 
 // спрайтов. Его основная задача - пересортировать порядок отрисовки спрайтов по isoY x isoX 
 
class IsoScene extends Sprite
{
	var isoChildrens:Array<IsoSprite>;
	var grid:Grid;
	var isoContainer:Sprite;
	var topContainer:Sprite;
	var isoHelper:IsoHelper;
	
	public function new(isoHelper:IsoHelper) 
	{
		super();
		this.isoHelper = isoHelper;
		
		var backData = Assets.getBitmapData("img/720_icylake.jpg");
		var backBitmap = new Bitmap(backData);
		addChild(backBitmap);
		
		grid = new Grid(isoHelper);
		addChild(grid);
		
		isoChildrens = new Array<IsoSprite>();
		isoContainer = new Sprite();
		addChild(isoContainer);
		
		topContainer = new Sprite();
		addChild(topContainer);
	}
		
	public function addIsoChild(item:IsoSprite)
	{			
		isoContainer.addChild(item);
		isoChildrens.push(item);

		item.addEventListener(UnitDragEvent.START, onDragStart);
		item.addEventListener(UnitDragEvent.MOVE, onDrag);
		item.addEventListener(UnitDragEvent.END, onDragEnd);
	}
	
	public function onDrag(e:UnitDragEvent):Void {			
		var unit:Unit = cast e.target;
		grid.movePositionRectangle(e.getIsoPos());
				
		var grid_pos =  grid.gridTruncation(e.getIsoPos());		
		var place:FirePlace = grid.firePlaces.findPlace(grid_pos);
		
		if (place != null) {			
			unit.setColorOverlay(0.25, 1.25, 0.25, 0, 50);	
			unit.direction = place.getUnitDirection();
			grid.drawFireArea(unit.getAttackArea(place));
		}else {
			unit.setColorOverlay(1.25, 0.25, 0.25, 50);
			grid.clearFireArea();
		}
	}
	
	public function onDragStart(e:UnitDragEvent):Void {
			var target:Unit = e.target;
			isoContainer.removeChild(target);
			topContainer.addChild(target);
			
			grid.showGridCursor();
			grid.showFirePlaces();
			grid.movePositionRectangle(e.getIsoPos());
	}
	
	public function onDragEnd(e:UnitDragEvent):Void {			
			var target:Unit = e.target;
			topContainer.removeChild(target);
			isoContainer.addChild(target);
			grid.hideGridCursor();	
			grid.hideFirePlaces();
									
			var grid_pos =  grid.gridTruncation(e.getIsoPos());		
			var new_place:FirePlace = grid.firePlaces.findPlace(grid_pos);
			
			var target_pos =  grid.gridTruncation(target.isoPos);		
			var old_place:FirePlace = grid.firePlaces.findPlace(target_pos);
			
			if (new_place != null) {				
				if (new_place.unit != null) {
					new_place.unit.isoPos = target.isoPos;
					if (old_place != null) {
						old_place.unit = new_place.unit;
						new_place.unit.direction = old_place.getUnitDirection();
					}
				}else {
					if (old_place != null) {
						old_place.unit = null;
					}
				}
				target.isoPos = grid.recalcCellPos(e.getIsoPos());
				new_place.unit = target;				
				updateIsoOrder();
			}else {
				// костыль с переустановкой iso значения позволяет включить
				// обновление экранных координат, сдвинутых при перемещении спрайта мышкой
				target.isoPos = target.isoPos;
			}
			grid.clearFireArea();
			target.clearColorOverlay();
	}
	
	public function updateIsoOrder() {
		isoChildrens.sort(sortByIsoYX);
		var i:Int;
		for (i in 0...isoChildrens.length) {
			isoContainer.setChildIndex(isoChildrens[i], i);
		}
	}
	
	private function sortByIsoYX(a:IsoSprite, b:IsoSprite):Int {
		if (a.isoPos.y == b.isoPos.y) {
			if (a.isoPos.x == b.isoPos.x) return 0;
			if (a.isoPos.x > b.isoPos.x) return 1;
			else return -1;
		}else if (a.isoPos.y > b.isoPos.y) return 1;		
		return -1;
	}
}