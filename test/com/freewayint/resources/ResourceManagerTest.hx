package com.freewayint.resources;

import com.freewayint.resources.ResourceManager;
import com.freewayint.spritesheet.Spritesheet;
import haxe.unit.TestCase;
import openfl.Assets;


class ResourceManagerTest extends haxe.unit.TestCase
{
	public function testSingletone():Void
	{
		var firstInstance = ResourceManager.getInstance();
		var secondInstance = ResourceManager.getInstance();
		assertEquals(firstInstance, secondInstance);
	}
	
	public function testBitmapGetting():Void 
	{
		var mngr = ResourceManager.getInstance();
		var bitmap = mngr.getBitmap("other/test.png");
		assertTrue(bitmap != null);
	}
	
	public function testJsonGetting():Void 
	{
		var mngr = ResourceManager.getInstance();
		var json = mngr.getJson("other/test.json");
		assertTrue(json != null);
	}
}