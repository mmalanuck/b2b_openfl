<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>4.0.1</string>
        <key>fileName</key>
        <string>D:/develop/b2b_openfl/ss/gunner_tl.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json-array</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <true/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../assets/img/cosmo/artillery/gunner_tl.json</filename>
            </struct>
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0000.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0001.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0002.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0003.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0004.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0005.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0006.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0007.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0008.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0009.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0010.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0011.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0012.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0013.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0014.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/idle/gunner_tl_0015.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0016.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0017.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0018.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0019.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0020.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0021.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0022.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0023.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0024.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0025.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0026.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0027.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0028.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0029.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0030.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0031.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0032.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0033.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0034.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0035.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0036.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0037.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0038.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0039.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0040.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0041.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0042.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0043.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0044.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/fire/gunner_tl_0045.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0046.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0047.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0048.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0049.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0050.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0051.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0052.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0053.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0054.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0055.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0056.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0057.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0058.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0059.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/hit/gunner_tl_0060.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0061.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0062.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0063.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0064.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0065.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0066.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0067.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0068.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0069.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0070.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0071.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0072.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0073.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0074.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0075.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0076.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0077.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0078.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0079.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0080.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0081.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0082.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0083.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0084.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0085.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0086.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0087.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0088.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0089.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0090.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0091.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0092.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0093.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0094.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0095.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0096.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0097.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0098.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/salute/gunner_tl_0099.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0100.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0101.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0102.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0103.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0104.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0105.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0106.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0107.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0108.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0109.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0110.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0111.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0112.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0113.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0114.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0115.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0116.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0117.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0118.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0119.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/GUNNER/die/gunner_tl_0120.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
