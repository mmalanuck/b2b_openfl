package com.freewayint.iso;
import openfl.display.Sprite;
import openfl.geom.Point;

/**
 * ...
 * @author cjay
 */

class IsoSprite extends Sprite
{
	public var isoPos(get, set):Point;	
	
	var _isoPos:Point;	
	var isoHelper:IsoHelper;
	
	// properties
	function get_isoPos():Point { return _isoPos; }	
	function set_isoPos(value:Point):Point { 
		_isoPos = value;
		var local = isoHelper.isoToScreen(value);
		x = local.x;
		y = local.y;
		return _isoPos;
	}
	
	public function new(isoHelper:IsoHelper) 
	{
		super();
		_isoPos = new Point();
		this.isoHelper = isoHelper;
	}
}