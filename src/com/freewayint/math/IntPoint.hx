package com.freewayint.math;

/**
 * ...
 * @author cjay
 */
class IntPoint{
	var x:Int;
	var y:Int;
	
	public function new(x:Int, y:Int) {
		this.x = x;
		this.y = y;		
	}
}