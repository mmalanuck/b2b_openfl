package com.freewayint.animations;
import openfl.events.Event;

/**
 * ...
 * @author cjay
 */

class AnimationEvent extends Event
{
	public static var START:String = "AnimationEventSTART";
	public static var END:String = "AnimationEventEND";
	
	private var state:Animation;
	
	public function new(eventType:String,state:Animation, bubbles:Bool = false, cancelable:Bool = false ) {
		super(eventType, bubbles, cancelable);
		this.state = state;
	}
	  
	public function getState():Animation{
		return state;
	}
}