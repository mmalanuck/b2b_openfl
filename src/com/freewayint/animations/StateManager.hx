package com.freewayint.animations;
import com.freewayint.animations.IsoAnimation;

/**
 * ...
 * @author cjay
 */
class StateManager
{
	var _states:Map<String, State>;
	
	public function new() 
	{
		_states = new Map<String, State>();
	}
	
	public function addState(name:String, animation:IsoAnimation) 
	{
		_states.set(name, new State(name, animation));
	}
	
	public function get(name:String):State
	{
		return _states.get(name);
	}
	
	public function randomState():State
	{
		var array = Lambda.array(_states);
		var key = Math.floor(Math.random() * array.length); 
		
		return array[ key ];
	}
	
}