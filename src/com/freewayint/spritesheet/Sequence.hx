package com.freewayint.spritesheet;
import openfl.display.BitmapData;

/**
 * ...
 * @author cjay
 */
class Sequence {
	var name:String;
	public var items:Array<SpriteCrop> = new Array<SpriteCrop>();
	public var bitmapData:BitmapData;
	public var width:Int;
	public var height:Int;
	
	public function new(pName:String, pBitmap:BitmapData, pWidth:Int, pHeight:Int) {
		name = pName;
		bitmapData = pBitmap;
		width = pWidth;
		height = pHeight;
	}
	
	public function add(crop: SpriteCrop) {
		for (item in items)
			if (item.frame == crop.frame)
				return;
				
		items.push(crop);
	}
	
	public function count():Int 
	{
		return items.length;
	}
}