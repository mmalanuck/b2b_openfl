package com.freewayint.resources;

import haxe.Resource;
import openfl.Assets;
import openfl.display.BitmapData;
import openfl.display.Tilesheet;
import openfl.utils.Dictionary;


/**
 * ...
 * @author cjay
 */
class ResourceManager
{
	static private var _INSTANCE : ResourceManager;
	
	var bitmaps:Map<String, BitmapData>;
	//var json_frames:Map<String, Array<TexturePackerFrame>>;
	var tilesheets:Map<String, Tilesheet>;
	var jsons:Map<String, Dynamic>;
		
	public function new() 
	{
		bitmaps = new Map<String, BitmapData>();
		//json_frames = new Map<String, Array<TexturePackerFrame>>();
		tilesheets = new Map<String, Tilesheet>();
		jsons = new Map<String, Dynamic>();
	}
	
	static public function getInstance():ResourceManager {
     return (_INSTANCE != null) ? 
       _INSTANCE : 
       _INSTANCE = new ResourceManager();
    }
	
	public function getBitmap(path:String):BitmapData {
		if (bitmaps.exists(path))
			return bitmaps.get(path);
		
		var bitmap = openfl.Assets.getBitmapData(path);
		if (bitmap == null) 			
			throw "Image file '" +path+"' not found";		
			
		bitmaps.set(path, bitmap);
		return bitmap;
	}
	
	public function getJson(path:String):Dynamic{
		if (jsons.exists(path))
			return jsons[path];
					
		var file = Assets.getText(path);
		var json = haxe.Json.parse(file);
		jsons[path] = json;
		return json;
	}
	
	public function getTileSheet(json_path:String, bitmap_path:String):Tilesheet {
		if (tilesheets.exists(json_path))
			return tilesheets.get(json_path);
		
		var tile = new Tilesheet(this.getBitmap(bitmap_path));
		
		tilesheets.set(json_path, tile);
		return tile;
	}
	
}