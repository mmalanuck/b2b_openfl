package com.freewayint.spritesheet;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.geom.Matrix;
import openfl.geom.Rectangle;

/**
 * ...
 * @author cjay
 */
class Spritesheet extends Sprite
{	
	public var frame(default, set):Int = 0;
	public var animationName:String;
	
	var animation:Sequence;
	var crops:Array<SpriteCrop>;
	var bitmapData:BitmapData;
	var spriteSet:SpriteFabric;	
	
	public function new(fabric:SpriteFabric) 
	{
		super();
		spriteSet = fabric;		
		var maxSize = fabric.getMaxSize();
		
		bitmapData = new BitmapData(maxSize.width, maxSize.height, true, 0x00000000);
		var bitmap = new Bitmap(bitmapData);
		addChild(bitmap);
		
		// TODO: переработать назначение анимаций
		setAnimation("drone_anim_br");
	}
	
	function set_frame(value:Int):Int {
		if (frame == value) 
			return frame;
		
		frame = value;
		updateFrame(frame );
		return frame;
	}
	
	public function setAnimation(anim:String):Void {
		animationName = anim;
		animation  = spriteSet.animations[anim];		
		crops = animation.items;				
	}
	
	public function updateFrame(p_frame:Int):Void
	{				
		var currentCrop = crops[p_frame];
		bitmapData.copyPixels(animation.bitmapData, currentCrop.area, currentCrop.offset);
	}
	
}