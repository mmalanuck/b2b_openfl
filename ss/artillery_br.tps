<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>4.0.1</string>
        <key>fileName</key>
        <string>D:/develop/b2b_openfl/ss/artillery_br.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json-array</string>
        <key>textureFileName</key>
        <filename>../assets/img/cosmo/artillery_br.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <true/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../assets/img/cosmo/artillery/artillery_br.json</filename>
            </struct>
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0131.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0132.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0133.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0134.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0135.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0136.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0137.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0138.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0139.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0140.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0141.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0142.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0143.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0144.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0145.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0146.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0147.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0148.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0149.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0150.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0151.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0152.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0153.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0154.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0155.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0156.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0157.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0158.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0159.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0160.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0161.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0162.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0163.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0164.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/die/artillery_br_0165.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0017.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0018.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0019.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0020.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0021.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0022.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0023.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0024.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0025.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0026.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0027.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0028.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0029.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0030.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0031.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0032.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0033.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0034.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0035.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0036.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0037.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0038.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/fire/artillery_br_0039.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0116.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0117.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0118.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0119.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0120.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0121.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0122.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0123.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0124.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0125.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0126.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0127.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0128.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0129.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/hit/artillery_br_0130.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0000.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0001.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0002.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0003.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0004.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0005.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0006.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0007.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0008.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0009.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0010.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0011.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0012.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0013.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0014.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/idle/artillery_br_0015.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0040.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0041.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0042.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0043.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0044.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0045.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0046.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0047.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0048.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0049.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0050.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0051.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0052.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0053.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0054.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0055.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0056.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0057.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0058.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0059.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0060.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0061.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0062.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0063.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0064.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0065.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0066.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0067.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0068.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0069.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0070.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0071.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0072.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0073.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0074.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0075.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0076.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0077.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0078.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0079.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0080.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0081.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0082.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0083.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/reload/artillery_br_0084.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0085.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0086.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0087.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0088.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0089.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0090.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0091.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0092.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0093.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0094.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0095.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0096.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0097.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0098.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0099.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0100.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0101.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0102.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0103.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0104.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0105.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0106.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0107.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0108.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0109.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0110.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0111.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0112.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0113.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0114.png</filename>
            <filename>../../b2b2b/sw/freeway/!BACK2BACK_REBIRTH/!BACK2BACK_REBIRTH/UNITS/COSMO/ARTILLERY/salute/artillery_br_0115.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
