package com.freewayint.iso;
import openfl.geom.Matrix;
import openfl.geom.Point;

/**
 * ...
 * @author cjay
 */
class IsoHelper
{
	public var toScreenMatrix:Matrix;
	public var toIsoMatrix:Matrix;
	
	public function new() 
	{
		toScreenMatrix = new Matrix(1, 0.58, -1, 0.58, 728 / 2, 546 / 2);		
		toIsoMatrix = toScreenMatrix.clone();
		toIsoMatrix.invert();
	}
	
	public function isoToScreen(pos:Point):Point {
		return toScreenMatrix.transformPoint(pos);
	}
	
	public function screenToIso(pos:Point):Point {
		return toIsoMatrix.transformPoint(pos);
	}
}