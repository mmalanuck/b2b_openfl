package com.freewayint;

import com.freewayint.animations.AnimationEvent;
import com.freewayint.elements.Grid;
import com.freewayint.iso.IsoHelper;
import com.freewayint.iso.IsoScene;
import com.freewayint.resources.ResourceManager;
import com.freewayint.spritesheet.SpriteFabric;
import com.freewayint.spritesheet.Spritesheet;
import com.freewayint.units.Enemy;
import com.freewayint.units.Unit;
import com.freewayint.units.UnitDragEvent;
import haxe.io.Bytes;
import haxe.macro.Compiler;
import openfl.Assets;
import openfl.Lib;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.KeyboardEvent;
import openfl.events.MouseEvent;
import openfl.geom.Point;
import openfl.ui.Keyboard;


/**
 * ...
 * @author cjay
 */
class Main extends Sprite 
{
    var lastTime:Int = 0;
	var unitsList:Array<Unit>;
	var enemyList:Array<Enemy>;
	var isoScene:IsoScene;
	var isoHelper:IsoHelper;
	
	var testSprite:Spritesheet;
	
	var cFrame:Float = 0;
	
    public function new()
    {
        super();
		unitsList = new Array<Unit>();		
		enemyList = new Array<Enemy>();	
		isoHelper = new IsoHelper();
        addEventListener(Event.ADDED_TO_STAGE, onAdded);
    }

    function onAdded(e)
    {
        removeEventListener(Event.ADDED_TO_STAGE, onAdded);
        stage.addEventListener(Event.RESIZE, onResize);
        init();
    }

    function onResize(e)
    {
        // else (resize or orientation change)
    }
	
    function init() {				
		isoScene = new IsoScene(isoHelper);
		addChild(isoScene);
		
		var cosmo :Unit;
		var _rm = ResourceManager.getInstance();
		var fabric = new SpriteFabric(_rm);
		fabric.load("other/test_anim.json");
		
		testSprite = fabric.createSpritesheet();
		testSprite.x = 100;
		testSprite.y = 200;
		
		addChild(testSprite);
		
		for (i in 0...4) {
			cosmo = new Unit(isoHelper);
			cosmo.isoPos = new Point(20*(Std.random(20)-10), 20*(Std.random(20)-10));
			//isoScene.addIsoChild(cosmo);
			//unitsList.push(cosmo);
		}
		
		var enemy: Enemy;
		for (i in 0...4) {
			enemy = new Enemy(isoHelper);
			enemy.isoPos = new Point(20*(Std.random(20)-10), 20*(Std.random(20)-10));
			//isoScene.addIsoChild(enemy);
			//enemyList.push(enemy);
		}
		
		
		var fabric = new SpriteFabric(ResourceManager.getInstance());
		
		
		addEventListener(Event.ENTER_FRAME, onEnterFrame);		
    }
	
    public function onEnterFrame(e:Event):Void
    {
        var delta = Lib.getTimer()- lastTime;
        lastTime = Lib.getTimer();
		
		cFrame = cFrame + 0.5;
		if (cFrame > 15)
			cFrame = 0;
		
		testSprite.frame = Math.ceil(0 + cFrame);
		
		for (i in unitsList) {
			i.update(delta);
		}
		for (i in enemyList) {
			i.update(delta);
		}
		for (i in unitsList) {
			i.draw();
		}
		for (i in enemyList) {
			i.draw();
		}
    }    
}
