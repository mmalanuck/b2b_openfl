package com.freewayint.animations;

import com.freewayint.animations.AnimationEvent;
import com.freewayint.units.UnitDragEvent;
import openfl.Assets;
import openfl.display.Graphics;
import openfl.display.Tilesheet;
import openfl.events.EventDispatcher;
import openfl.media.Sound;

/**
 * ...
 * @author cjay
 */

class Animation extends EventDispatcher
{
	public var startFrame:Int;
	public var endFrame:Int;
	public var loop:Bool;
	public var rewerse:Bool;
	public var length:Int;
	public var frame:Int;
	public var tile:Tilesheet;
	
	var forward:Bool;
	var innerTimer:Int;
	var fps = 40; // 1000 ms  / 25 fps	
	
	public function new(tile:Tilesheet=null, start:Int=0, end:Int=0, loop = false, rewerse = false) {
		super();
		this.tile = tile;
		this.startFrame = start;
		this.endFrame = end;
		this.loop = loop;
		this.rewerse = rewerse;
		this.length = endFrame - startFrame + 1;
		this.forward = true;
	}
	
	function onStartAnimation() {
		dispatchEvent(new AnimationEvent(AnimationEvent.START, this));
	}
	
	public function start() {
		frame = startFrame;
		innerTimer = 0;		
		onStartAnimation();
	}
	
	public function seedAnimation() {
		innerTimer = Std.random(length) * 40;
	}
	
	public function update(delta:Int) 
	{
		innerTimer += delta;		
		
		frame = Std.int(innerTimer / fps) + startFrame;
		if (frame > endFrame ) {
			if (loop) {
				if (rewerse) {
					if (forward) {
						frame = endFrame;
					}else {
						frame = startFrame;
					}
					forward = !forward;
				}else {
					frame = startFrame;
				}				
			}else {
				frame = endFrame;
			}
			innerTimer = innerTimer % fps;			
			dispatchEvent(new AnimationEvent(AnimationEvent.END, this));
		}
		if (!forward) {
			frame = endFrame - Std.int(innerTimer / fps);
		}
	}
	
	public function draw(graphics:Graphics):Void {
		if (tile == null) return;
				
		var drawList = [ -125, -130, frame * 1.0];
		tile.drawTiles(graphics, drawList);
	}
}