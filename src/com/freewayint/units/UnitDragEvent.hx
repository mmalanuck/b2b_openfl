package com.freewayint.units;
import openfl.events.Event;
import openfl.geom.Point;

/**
 * ...
 * @author cjay
 */

class UnitDragEvent extends Event
{
	public static var MOVE = "UnitDragEventMOVE";
	public static var START = "UnitDragEventSTART";
	public static var END = "UnitDragEventEND";
	
	private var pos:Point;
	private var isoPos:Point;
	
	public function new(type:String, pos:Point, isoPos:Point, bubbles:Bool = false, cancelable:Bool = false ) {
		super(type, bubbles, cancelable);
		this.pos = pos;
		this.isoPos = isoPos;
	}
	  
	public function getPos():Point{
		return pos;
	}
	public function getIsoPos():Point{
		return isoPos;
	}
}