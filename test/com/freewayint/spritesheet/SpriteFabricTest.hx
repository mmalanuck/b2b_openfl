package com.freewayint.spritesheet;

import com.freewayint.resources.ResourceManager;
import com.freewayint.spritesheet.Sequence;
import com.freewayint.spritesheet.SpriteFabric;
import com.freewayint.spritesheet.Spritesheet;
import haxe.unit.TestCase;
import openfl.Assets;


class SpriteFabricTest extends haxe.unit.TestCase
{
	var _rm:ResourceManager;
	
	override public function setup() {
		_rm= ResourceManager.getInstance();
	}
	
	public function testLoadAnimationSuccess():Void
	{
		var fabric = new SpriteFabric(_rm);
		fabric.load("other/test_anim.json");		
		
		checkAnimationsCount(2, fabric);
		checkFramesCount(34, fabric);
	}
	
	public function testLoadSecondAnimationSuccess():Void
	{
		var fabric = new SpriteFabric(_rm);
		fabric.load("other/test_anim.json");
		fabric.load("other/test_anim_2.json");
		
		checkAnimationsCount(4, fabric);
		checkFramesCount(68, fabric);
	}
	
	public function testIgnoreAlreadyLoadedAnimations():Void
	{
		var fabric = new SpriteFabric(_rm);
		fabric.load("other/test_anim.json");
		fabric.load("other/test_anim_2.json");		
		checkAnimationsCount(4, fabric);
		checkFramesCount(68, fabric);
		
		fabric.load("other/test_anim.json");
		checkAnimationsCount(4, fabric);
		checkFramesCount(68, fabric);
		
		fabric.load("other/test_anim_2.json");		
		checkAnimationsCount(4, fabric);
		checkFramesCount(68, fabric);
	}
	
	function checkAnimationsCount(expected:Int, fabric:SpriteFabric):Void 
	{
		var count = 0;
		for (animName in fabric.animations.keys())
			count += 1;
			
		assertEquals(expected, count);
	}
	
	function checkFramesCount(expected: Int, fabric:SpriteFabric):Void 
	{
		var count = 0;
		for (animation in fabric.animations) 
			count += animation.count();
		
		assertEquals(expected, count);
	}
}