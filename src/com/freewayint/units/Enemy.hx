package com.freewayint.units;
import bitmapFont.BitmapTextField;
import com.freewayint.animations.IsoAnimation;
import com.freewayint.animations.State;
import com.freewayint.animations.StateManager;
import com.freewayint.elements.FireArea;
import com.freewayint.elements.FirePlace;
import com.freewayint.iso.IsoHelper;
import com.freewayint.iso.IsoSprite;
import com.freewayint.animations.Animation;
import com.freewayint.animations.AnimationEvent;
import com.freewayint.resources.ResourceManager;
import haxe.ds.StringMap;
import openfl.display.Sprite;
import openfl.display.Tilesheet;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.MouseEvent;
import openfl.geom.ColorTransform;
import openfl.geom.Point;


/**
 * ...
 * @author cjay
 */
 
class Enemy extends IsoSprite
{
	public var direction(get, set) :Int;
	public var Speed:Point = new Point();
	
	
	private function get_direction():Int 
	{
		return _direction;
	}
	
	private function set_direction(val:Int):Int
	{
		if (val == _direction)
			return val;
			
		_direction = val;
		
		if (_direction == 0)
			Speed = new Point(0, 0.025);
		
		if (_direction == 1)
			Speed = new Point(0.025, 0);
			
		if (_direction == 2)
			Speed = new Point(-0.025, 0);
			
		if (_direction == 3)
			Speed = new Point(0, -0.025);
		
		return _direction;
	}
	
	public var current_state:State;
	
	var _direction:Int = 0;
	var idMap:Map<String, Int>;
	var state_mamager:StateManager;	
	
	var label:BitmapTextField;
	
	public function new(isoHelper:IsoHelper) 
	{
		super(isoHelper);
		
		state_mamager = new StateManager();
		
		label = new BitmapTextField();
		label.y = 0;
		label.size = 1.5;		
		addChild(label);
		
		direction = Std.random(4);
		loadStates();
	}
	
	/// Инициализирует состояния с анимациями
	function loadStates():Void 
	{
		var res_man = ResourceManager.getInstance();
		var load_tile = function(name:String, prefix:String) return res_man.getTileSheet(
																	prefix + name + ".json", prefix + name + ".png");
		
		var anim_1 = load_tile("anim_1", "img/enemy/droid_");
		var anim_2 = load_tile("anim_2", "img/enemy/droid_");
		var die_1 = load_tile("die_1", "img/enemy/droid_");
		var die_2 = load_tile("die_2", "img/enemy/droid_");
		var die_3 = load_tile("die_3", "img/enemy/droid_");
		var die_4 = load_tile("die_4", "img/enemy/droid_");
		var fire_1 = load_tile("fire_1", "img/enemy/droid_");
		var fire_2 = load_tile("fire_2", "img/enemy/droid_");
		
		
		var isoAnim = new IsoAnimation(16, true, false);
		isoAnim.addAnimation(anim_1, 0);
		isoAnim.addAnimation(anim_1, 17);
		isoAnim.addAnimation(anim_2, 0);
		isoAnim.addAnimation(anim_2, 17);		
		state_mamager.addState("anim", isoAnim);
		
		isoAnim = new IsoAnimation(50, true, true);
		isoAnim.addAnimation(die_1, 0);			
		isoAnim.addAnimation(die_2, 0);
		isoAnim.addAnimation(die_3, 0);
		isoAnim.addAnimation(die_4, 0);
		state_mamager.addState("die", isoAnim);
		
		isoAnim = new IsoAnimation(40);		
		isoAnim.addAnimation(fire_1, 0);
		isoAnim.addAnimation(fire_1, 41);
		isoAnim.addAnimation(fire_1, 82);
		isoAnim.addAnimation(fire_2, 0);		
		state_mamager.addState("fire", isoAnim);
				
		current_state = state_mamager.get("die");
	}
	
	public function draw()
	{
		graphics.clear();
		current_state.animation.direction = direction;
		current_state.animation.draw(graphics);		
	}
	
	public function update(delta:Int)
	{	
		var states_names = ["die", "anim", "fire"];
		
		if (Std.random(1000) > 990) {
			direction = Std.random(4);
		}
		
		if (Std.random(10000) > 9900) {			
			current_state = state_mamager.randomState();
		}
		
		label.text = current_state.animation.frame+"";
		
		if (current_state.name == "anim") {
			var speed = new Point(Speed.x * delta, Speed.y * delta);
			isoPos = isoPos.add(speed);
		}
		
		current_state.update(delta);		
	}	
}