package com.freewayint.animations;

/**
 * ...
 * @author cjay
 */
class State
{
	public var name(get, null):String;
	public var animation(get, null):IsoAnimation;
	
	var _name:String;
	var _animation:IsoAnimation;
	
			
	public function new(name:String, animation: IsoAnimation) 
	{
		this._name = name;
		this._animation = animation;
	}
	
	public function update(delta:Int):Void
	{
		_animation.update(delta);
	}
	
	function get_name():String 
	{
		return _name;
	}
	
	function get_animation():IsoAnimation 
	{
		return _animation;
	}
	
}