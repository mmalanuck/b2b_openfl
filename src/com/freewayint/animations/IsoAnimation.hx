package com.freewayint.animations;
import openfl.display.Graphics;
import openfl.display.Tilesheet;
import openfl.events.EventDispatcher;

/**
 * ...
 * @author cjay
 */
class IsoAnimation extends Animation
{
	public var direction:Int;
	
	var animations:Array<{tile:Tilesheet, frame:Int}> = [];
	
	public function new(length:Int, loop = false, rewerse = false) 
	{
		super(0, length, loop, rewerse);
		this.length = length;
		this.loop = loop;
		this.rewerse = rewerse;
		this.forward = true;
	}
	
	public function addAnimation(tile:Tilesheet, startFrame:Int):Void 
	{
		var params = { tile: tile, frame: startFrame }
		animations.push(params);
	}
	
	public function getImage(direction:Int):Tilesheet {
		return animations[direction].tile;
	}
	
	override public function draw(graphics:Graphics):Void {
		var animation = animations[this.direction];
		var drawList = [ -125, -130, frame * 1.0 + animation.frame];
		animation.tile.drawTiles(graphics, drawList);
	}
		
}