package;
import com.freewayint.resources.ResourceManagerTest;
import com.freewayint.spritesheet.SpriteFabricTest;



/**
 * ...
 * @author cjay
 */

class MainTest
{
	public static function main() 
	{				
		var r = new haxe.unit.TestRunner();
		r.add(new ResourceManagerTest());
		r.add(new SpriteFabricTest());
		r.run();
	}
}