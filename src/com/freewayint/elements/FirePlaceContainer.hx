package com.freewayint.elements;

import com.freewayint.elements.FirePlace;
import openfl.display.Sprite;
import openfl.geom.Point;

/**
 * ...
 * @author cjay
 */
class FirePlaceContainer extends Sprite
{
	var places:Array<FirePlace>;
	var cellSize:Int;
	
	public function new(cellSize = 30) 
	{
		super();
		this.cellSize = cellSize;
		places = new Array<FirePlace>();
		
		var place = new FirePlace(new Point(0, 2), 4);
		places.push(place);
		place = new FirePlace(new Point(-1, 2), 4);
		places.push(place);
		place = new FirePlace(new Point(1, 2), 4);
		places.push(place);
		
		place = new FirePlace(new Point(2, 0), 2);
		places.push(place);
		
		place = new FirePlace(new Point(2, 1), 2);
		places.push(place);
		
		place = new FirePlace(new Point(2, -1), 2);
		places.push(place);
		
		place = new FirePlace(new Point(-2, 0), 8);
		places.push(place);
		
		place = new FirePlace(new Point(-2, 1), 8);
		places.push(place);
		
		place = new FirePlace(new Point(-2, -1), 8);
		places.push(place);
		
		generatePlaces();
	}
	
	function generatePlaces() {
		graphics.clear();
		graphics.beginFill(0x44AEF4, 1);	
		for (place in places) {
			var pos_x = place.x * cellSize - cellSize/2;
			var pos_y = place.y * cellSize - cellSize/2;
			
			this.graphics.drawRect(pos_x, pos_y, cellSize, cellSize);
		}
		graphics.endFill();
	}
	
	public function findPlace(pos:Point):FirePlace {
		var result:FirePlace = null;
		
		var pos_x = Std.int(pos.x);
		var pos_y = Std.int(pos.y);
		
		for (place in places) {
			if ((place.x == pos_x) && (place.y == pos_y)) {
				result = place;
				break;
			}
		}
		return result;
	}
}
