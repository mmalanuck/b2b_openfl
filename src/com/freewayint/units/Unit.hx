package com.freewayint.units;
import bitmapFont.BitmapTextField;
import com.freewayint.animations.IsoAnimation;
import com.freewayint.animations.State;
import com.freewayint.animations.StateManager;
import com.freewayint.elements.FireArea;
import com.freewayint.elements.FirePlace;
import com.freewayint.iso.IsoHelper;
import com.freewayint.iso.IsoSprite;
import com.freewayint.animations.Animation;
import com.freewayint.animations.AnimationEvent;
import com.freewayint.resources.ResourceManager;
import haxe.ds.StringMap;
import openfl.display.Sprite;
import openfl.display.Tilesheet;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.MouseEvent;
import openfl.geom.ColorTransform;
import openfl.geom.Point;


/**
 * ...
 * @author cjay
 */
 
class Unit extends IsoSprite
{
	public var direction(default, default):Int;
	public var current_state:State;
	
	var idMap:Map<String, Int>;
	var state_mamager:StateManager;	
	
	var label:BitmapTextField;
	
	public function new(isoHelper:IsoHelper) 
	{
		super(isoHelper);
		label = new BitmapTextField();
		label.y = -100;
		label.size = 1.5;
		label.text = x+"x"+y;
		//addChild(label);
		
		state_mamager = new StateManager();
		direction = Std.random(4);
		
		loadStates();
		
		addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
	}
	
	public function getAttackArea(fireplace:FirePlace):Array<FireArea> {			
		var x_offset = 0;
		var y_offset = 0;
		var result:Array<FireArea> = new Array<FireArea>();
		if (fireplace.directions & 1 == 1)
			result = result.concat([for (i in 1...9) new FireArea(fireplace.x, fireplace.y - i, 1.0 / i)]);
		if (fireplace.directions & 2 == 2)
			result = result.concat([for (i in 1...9) new FireArea(fireplace.x + i, fireplace.y, 1.0 / i)]);
		if (fireplace.directions & 4 == 4)
			result = result.concat([for (i in 1...9) new FireArea(fireplace.x, fireplace.y + i, 1.0 / i)]);
		if (fireplace.directions & 8 == 8)
			result = result.concat([for (i in 1...9) new FireArea(fireplace.x - i, fireplace.y, 1.0 / i)]);
			
		return result;
	}
	
	function onMouseDown(event:MouseEvent)
	{
		alpha = 0.6;
		startDrag();
		
		var pos:Point = new Point(x, y);
		dispatchEvent(new UnitDragEvent(UnitDragEvent.START, pos, this.isoPos));
		stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);		
	}
	
	function onMouseMove(event:MouseEvent)
	{	
		var pos:Point = new Point(x, y);
		var iso:Point = isoHelper.screenToIso(pos);
		dispatchEvent(new UnitDragEvent(UnitDragEvent.MOVE, pos, iso));
	}
	
	function onMouseUp(event:MouseEvent) {	
		alpha = 1;
		stopDrag();
		var pos:Point = new Point(x, y);
		var iso:Point = isoHelper.screenToIso(pos);
		dispatchEvent(new UnitDragEvent(UnitDragEvent.END, pos, iso));				
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
	}
	
	/// Инициализирует состояния с анимациями
	function loadStates():Void 
	{
		var res_man = ResourceManager.getInstance();
		var load_tile = function(name:String, prefix:String) return res_man.getTileSheet(
																	prefix + name + ".json", prefix + name + ".png");
		var anim_tr = load_tile("tr", "img/cosmo/artillery_");		
		var anim_br = load_tile("br", "img/cosmo/artillery_");
		var anim_bl = load_tile("bl", "img/cosmo/artillery_");
		var anim_tl = load_tile("tl", "img/cosmo/artillery_");
		
		var isoAnim = new IsoAnimation(135);
		isoAnim.addAnimation(anim_tr, 30);
		isoAnim.addAnimation(anim_br, 30);
		isoAnim.addAnimation(anim_bl, 30);
		isoAnim.addAnimation(anim_tl, 30);
		state_mamager.addState("die", isoAnim);
		
		// Idle animation
		isoAnim = new IsoAnimation(15, true, true);
		isoAnim.addAnimation(anim_tr, 0);
		isoAnim.addAnimation(anim_br, 0);
		isoAnim.addAnimation(anim_bl, 0);
		isoAnim.addAnimation(anim_tl, 0);
		state_mamager.addState("idle", isoAnim);
		
		// fire animation
		isoAnim = new IsoAnimation(23);
		isoAnim.addAnimation(anim_tr, 17);
		isoAnim.addAnimation(anim_br, 17);
		isoAnim.addAnimation(anim_bl, 17);
		isoAnim.addAnimation(anim_tl, 17);
		state_mamager.addState("fire", isoAnim);
		
		// reload
		isoAnim = new IsoAnimation(45);
		isoAnim.addAnimation(anim_tr, 40);
		isoAnim.addAnimation(anim_br, 40);
		isoAnim.addAnimation(anim_bl, 40);
		isoAnim.addAnimation(anim_tl, 40);
		state_mamager.addState("reload", isoAnim);
		// salute
		isoAnim = new IsoAnimation(30);
		isoAnim.addAnimation(anim_tr, 85);
		isoAnim.addAnimation(anim_br, 85);
		isoAnim.addAnimation(anim_bl, 85);
		isoAnim.addAnimation(anim_tl, 85);
		state_mamager.addState("salute", isoAnim);
		
		// hit
		isoAnim = new IsoAnimation(15);
		isoAnim.addAnimation(anim_tr, 115);
		isoAnim.addAnimation(anim_br, 115);
		isoAnim.addAnimation(anim_bl, 115);
		isoAnim.addAnimation(anim_tl, 115);
		state_mamager.addState("hit", isoAnim);
		
		current_state = state_mamager.get("idle");
	}
	
	public function setColorOverlay(r=1.0,g=1.0,b=1.0,r_offset=0,g_offset=0,b_offset=0) {		
		transform.colorTransform = new ColorTransform(r, g, b, 1, r_offset, g_offset, b_offset, 0);
	}
	
	public function clearColorOverlay() {
		transform.colorTransform = new ColorTransform(1, 1, 1, 1, 0, 0, 0, 0);
	}
	
	public function draw()
	{
		graphics.clear();
		current_state.animation.direction = direction;
		current_state.animation.draw(graphics);		
	}
	
	public function update(delta:Int)
	{		
		current_state.update(delta);
	}
}