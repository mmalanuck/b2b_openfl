package com.freewayint.elements;

import bitmapFont.BitmapTextField;
import com.freewayint.iso.IsoHelper;
import openfl.Vector;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.geom.Point;


/**
 * ...
 * @author cjay
 */

class Grid extends Sprite
{
	public var cellPos:Point;
	public var firePlaces:FirePlaceContainer;
	
	var gridSize = 30;
	var gridLength = 13;
	var halfGridSize :Float;
	var label:BitmapTextField;
	var gridSprite:Sprite;
	var gridCursor:Sprite;
	var isoHelper:IsoHelper;
	var fireArea:Sprite;
		
	public function new(isoHelper:IsoHelper) 
	{
		super();
		this.isoHelper = isoHelper;
		halfGridSize = gridSize / 2;
		init();
	}	
	
	function init() 
	{
		cellPos = new Point();		

		label = new BitmapTextField();
		label.text = "0x0";		
		label.size = 1.5;
				
		label.transform.matrix = isoHelper.toIsoMatrix;
		addChild(label);
		
		transform.matrix = isoHelper.toScreenMatrix;
		
		gridSprite = new Sprite();
		generateGrid(gridSprite);
		addChild(gridSprite);
		
		hideGrid();
		
		firePlaces = new FirePlaceContainer(gridSize);
		firePlaces.alpha = 0.6;		
		addChild(firePlaces);
		hideFirePlaces();
		
		fireArea = new Sprite();
		addChild(fireArea);
		
		gridCursor = new Sprite();
		addChild(gridCursor);		
	}
	
	public function drawFireArea(attackAreaList:Array<FireArea>) 
	{
		if (!fireArea.visible)
			fireArea.visible = true;
		fireArea.graphics.clear();
		fireArea.graphics.lineStyle( 1, 0xFFcc00, 1 );
		fireArea.graphics.beginFill(0x74b5c5, 1);	
		for (area in attackAreaList) {
			fireArea.graphics.drawRect(area.x*gridSize-halfGridSize, area.y*gridSize-halfGridSize, gridSize, gridSize);
		}
		
		fireArea.graphics.endFill();
	}
	
	public function clearFireArea() {
		fireArea.visible = false;
	}
	
	public function gridTruncation(pos:Point):Point 
	{
		var result = new Point();
		
		var offsetX = halfGridSize;
		var offsetY = halfGridSize;
		
		if ((pos.x < halfGridSize) && (pos.x > -halfGridSize)) {
			result.x = 0;
		}else {
			if (pos.x < -halfGridSize)
				offsetX = -halfGridSize;
				
			result.x = Std.int((pos.x + offsetX - (pos.x-offsetX)%gridSize)/gridSize) ;
		}
		
		if ((pos.y < halfGridSize) &&(pos.y > -halfGridSize)){
			result.y = 0;
		}else {
			if (pos.y < -halfGridSize) offsetY = -halfGridSize;	
			
			result.y = Std.int((pos.y + offsetY- (pos.y-offsetY)%gridSize)/gridSize);	
		}
		
		return result;
	}
	
	public function showGrid() {
		gridSprite.visible = true;
	}
	
	public function hideGrid() {
		gridSprite.visible = false;
	}
	
	public function showFirePlaces() {
		firePlaces.visible = true;
	}
	
	public function hideFirePlaces() {
		firePlaces.visible = false;
	}
	
	public function movePositionRectangle(pos:Point):Void {
		updateCell(pos.x, pos.y);		
	}
	
	public function recalcCellPos(pos:Point):Point {
		var result = new Point();
		
		var offsetX = halfGridSize;
		var offsetY = halfGridSize;
		
		if ((pos.x < halfGridSize) && (pos.x > -halfGridSize)) {
			result.x = 0;
		}else {
			if (pos.x < -halfGridSize) offsetX = -halfGridSize;	
			result.x = pos.x + offsetX - (pos.x-offsetX)%gridSize ;
		}
		if ((pos.y < halfGridSize) &&(pos.y > -halfGridSize)){
			result.y = 0;
		}else {
			if (pos.y < -halfGridSize) offsetY = -halfGridSize;	
			result.y = pos.y + offsetY- (pos.y-offsetY)%gridSize;	
		}
		return result;
	}
	
	public function updateCell(x:Float, y:Float) {
		var cellPos = recalcCellPos(new Point(x, y));
		updateGridCursor(cellPos);
	}
	
	function generateGrid(sprite:Sprite) {
		var commands1:Vector<Int> =  new Vector<Int>();
		var commands2:Vector<Int> =  new Vector<Int>();
		var commands3:Vector<Int> =  new Vector<Int>();
		var commands4:Vector<Int> =  new Vector<Int>();
		var data1 = new Vector<Float>();
		var data2 = new Vector<Float>();
		var data3 = new Vector<Float>();
		var data4 = new Vector<Float>();
		
		// Quad 1
		for (x in 0...gridLength) {
			commands1.push(4);
			data1.push(x * gridSize - halfGridSize);
			data1.push(0 - halfGridSize);
			
			data1.push(x * gridSize - halfGridSize);
			data1.push(0 - halfGridSize);
						
			data1.push(x * gridSize - halfGridSize);
			data1.push(gridLength * gridSize - halfGridSize);			
			commands1.push(2);
		}		
		for (y in 0...gridLength) {
			commands1.push(4);
			data1.push(- halfGridSize);
			data1.push(y * gridSize - halfGridSize);
			
			data1.push(- halfGridSize);
			data1.push(y * gridSize - halfGridSize);
						
			data1.push(gridLength * gridSize - halfGridSize);			
			data1.push(y * gridSize - halfGridSize);
			commands1.push(2);
		}
		
		// Quad 2
		for (x in -gridLength...0) {
			commands2.push(4);
			data2.push(x * gridSize - halfGridSize);
			data2.push(0 - halfGridSize);
			
			data2.push(x * gridSize - halfGridSize);
			data2.push(0 - halfGridSize);
						
			data2.push(x * gridSize - halfGridSize);
			data2.push(gridSize*gridLength - halfGridSize);			
			commands2.push(2);
		}		
		for (y in 0...gridLength) {
			commands2.push(4);
			data2.push(-gridLength * gridSize- halfGridSize);
			data2.push(y * gridSize - halfGridSize);
			
			data2.push(-gridLength * gridSize- halfGridSize);
			data2.push(y * gridSize - halfGridSize);
						
			data2.push(0 - halfGridSize);			
			data2.push(y * gridSize - halfGridSize);
			commands2.push(2);
		}
		// Quad 3
		for (x in 0...gridLength) {
			commands3.push(4);
			data3.push(x * gridSize - halfGridSize);
			data3.push(-gridLength * gridSize - halfGridSize);
			
			data3.push(x * gridSize - halfGridSize);
			data3.push(-gridLength * gridSize - halfGridSize);
						
			data3.push(x * gridSize - halfGridSize);
			data3.push(0 - halfGridSize);			
			commands3.push(2);
		}		
		for (y in -gridLength...0) {
			commands3.push(4);
			data3.push(- halfGridSize);
			data3.push(y * gridSize - halfGridSize);
			
			data3.push(- halfGridSize);
			data3.push(y * gridSize - halfGridSize);
						
			data3.push(gridLength * gridSize - halfGridSize);			
			data3.push(y * gridSize - halfGridSize);
			commands3.push(2);
		}
		
		// Quad 4
		for (x in -gridLength...0) {
			commands4.push(4);
			data4.push(x * gridSize - halfGridSize);
			data4.push(-gridLength * gridSize - halfGridSize);
			
			data4.push(x * gridSize - halfGridSize);
			data4.push(-gridLength * gridSize - halfGridSize);
						
			data4.push(x * gridSize - halfGridSize);
			data4.push(0 - halfGridSize);			
			commands4.push(2);
		}		
		for (y in -gridLength...0) {
			commands4.push(4);
			data4.push(-gridLength * gridSize- halfGridSize);
			data4.push(y * gridSize - halfGridSize);
			
			data4.push(-gridLength * gridSize- halfGridSize);
			data4.push(y * gridSize - halfGridSize);
						
			data4.push(0 - halfGridSize);			
			data4.push(y * gridSize - halfGridSize);
			commands4.push(2);
		}
		
		sprite.graphics.clear();	
		
		sprite.graphics.lineStyle( 1, 0xFF0000, 1 );
		sprite.graphics.drawPath(commands1, data1);
		
		sprite.graphics.lineStyle( 1, 0x00ff00, 1 );
		sprite.graphics.drawPath(commands2, data2);
		
		sprite.graphics.lineStyle( 1, 0x0000ff, 0.3 );
		sprite.graphics.drawPath(commands3, data3);
		
		sprite.graphics.lineStyle( 1, 0xff00ff, 0.3 );
		sprite.graphics.drawPath(commands4, data4);
		
		sprite.graphics.lineStyle( 1, 0xFFffff, 0.6 );
		
		sprite.graphics.moveTo( -gridLength * gridSize, 0 );
		sprite.graphics.lineTo( gridLength * gridSize, 0 );
		sprite.graphics.moveTo( 0,-gridLength * gridSize );
		sprite.graphics.lineTo( 0, gridLength * gridSize );
	}
	
	public function showGridCursor() {
		gridCursor.visible = true;
	}
	
	public function hideGridCursor() {
		gridCursor.visible = false;
	}
	
	function updateGridCursor(pos:Point) {				
		// Drawing Cell under dragging unit
		gridCursor.graphics.clear();
		gridCursor.graphics.lineStyle( 1, 0xFFcc00, 1 );
		gridCursor.graphics.beginFill(0x74b5c5, 1);			
		gridCursor.graphics.drawRect(pos.x-halfGridSize, pos.y-halfGridSize, gridSize, gridSize);
		gridCursor.graphics.endFill();
	}
}