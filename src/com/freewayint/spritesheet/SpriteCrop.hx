package com.freewayint.spritesheet;
import openfl.geom.Point;
import openfl.geom.Rectangle;

/**
 * ...
 * @author cjay
 */
class SpriteCrop { 
	
	public var animation: String;
	public var area: Rectangle; // cropped part of image
	public var frame: Int;
	public var offset:Point;
	public var width: Int;	// untrimmed image width
	public var height: Int; // untrimmed image height
	
	var sprite_source: Rectangle; // geom properties of trimmed piece
	
	public function new(?item:Dynamic) {
		if (item == null)
			return;
		
		animation = item.animation;
		frame = item.frame;
		area = new Rectangle(item.crop.x, item.crop.y, item.crop.w, item.crop.h);
		sprite_source = new Rectangle(item.spriteSourceSize.x, item.spriteSourceSize.y, item.spriteSourceSize.w, item.spriteSourceSize.h);
		offset = new Point(item.spriteSourceSize.x, item.spriteSourceSize.y);
		width = item.sourceSize.w;
		height = item.sourceSize.h;
	}
}