package com.freewayint.math;

/**
 * ...
 * @author cjay
 */
class Size
{
	public var width: Int;
	public var height: Int;
	
	public function new(width:Int, height:Int) 
	{
		this.width = width;
		this.height = height;
	}
	
}