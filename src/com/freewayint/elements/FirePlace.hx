package com.freewayint.elements;
import com.freewayint.units.Unit;
import openfl.geom.Point;

/**
 * ...
 * @author cjay
 */
class FirePlace
{
	public var directions:Int = 0;
	public var x:Int;
	public var y:Int;
	public var unit:Unit;
	
	public function new(pos:Point, directions:Int) 
	{
		this.directions = directions;
		this.x = Std.int(pos.x);
		this.y = Std.int(pos.y);
		unit = null;
	}
	
	public function getUnitDirection():Int {
		if ((directions & 8) == 8) return 3;
		if ((directions & 4) == 4) return 2;
		if ((directions & 2) == 2) return 1;
		return 0;
	}
}