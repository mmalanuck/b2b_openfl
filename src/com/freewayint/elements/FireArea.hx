package com.freewayint.elements;

/**
 * ...
 * @author cjay
 */
class FireArea
{
	public var x:Int;
	public var y:Int;
	public var percent:Float;
	public function new(x=0, y=0, percent=1.0) 
	{
		this.x = x;
		this.y = y;
		this.percent = percent;
	}
	
}