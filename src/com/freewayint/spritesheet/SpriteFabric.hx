package com.freewayint.spritesheet;
import com.freewayint.animations.Animation;
import com.freewayint.math.Size;
import com.freewayint.resources.ResourceManager;
import haxe.io.Path;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.geom.Point;

/**
 * ...
 * @author cjay
 */

class SpriteFabric 
{	
	var resource_manager:ResourceManager;
	public var animations = new Map<String,Sequence>();
	
	public function new(rm:ResourceManager) 
	{
		resource_manager = rm;
	}
	
	/**
	 * Валидация json-документа, описывающего 
	 * спрайтлист анимацию на 1 листе
	 * @param	json
	 */
	public function validateJson(json:Dynamic) {		
		if (json.size == null)
			throw "No 'size'-field in JSON";
			
		if (json.frames == null)
			throw "No frames in JSON";
			
		if (json.bitmap == null)
			throw "No 'bitmap'-field in JSON";
			
		var items = cast(json.frames, Array<Dynamic>);		
		for (item in items) {
			validateJsonItem(item);
		}
	}
	
	/**
	 * Проверка наличия необходимых полей в json-объекте,
	 * описывающем кадр на спрайтлисте
	 * @param	item - JSON-описание кадра
	 */
	public function validateJsonItem(item:Dynamic) {
		if (item.animation == null)
			throw "No 'animation'-field in JSON";
		if (item.frame == null)
			throw "No 'frame'-field in JSON";
		
		if (item.crop == null)
			throw "No 'crop'-field in JSON";
		if (item.crop.x == null)
			throw "No 'crop.x'-field in JSON";
		if (item.crop.y == null)
			throw "No 'crop.y'-field in JSON";
		if (item.crop.w == null)
			throw "No 'crop.w'-field in JSON";
		if (item.crop.h == null)
			throw "No 'crop.h'-field in JSON";
		
		if (item.spriteSourceSize == null)
			throw "No 'spriteSourceSize'-field in JSON";
		if (item.spriteSourceSize.x == null)
			throw "No 'spriteSourceSize.x'-field in JSON";
		if (item.spriteSourceSize.y == null)
			throw "No 'spriteSourceSize.y'-field in JSON";
		if (item.spriteSourceSize.w == null)
			throw "No 'spriteSourceSize.w'-field in JSON";
		if (item.spriteSourceSize.h == null)
			throw "No 'spriteSourceSize.h'-field in JSON";
			
		if (item.sourceSize == null)
			throw "No 'sourceSize'-field in JSON";
		if (item.sourceSize.w == null)
			throw "No 'sourceSize.w'-field in JSON";
		if (item.sourceSize.h == null)
			throw "No 'sourceSize.h'-field in JSON";
	}
	
	/**
	 * Загружает спрайт-анимацию из json-файла
	 * @param	path - путь к json-файлу
	 * @return	список спрайт-кадров
	 */
	function loadFrames(json:Dynamic):List<SpriteCrop> {
		validateJson(json);
		
		var items = cast(json.frames, Array<Dynamic>);
		var frames = new List<SpriteCrop>();
		for (item in items) {
			frames.add(new SpriteCrop(item));			
		}
		return frames;
	}
	
	/** 
	  		Загружаем анимацию из json-файла
	  		@param path - пусть к файлу
	 **/ 
	function loadAnimation(json: Dynamic, bitmap:BitmapData) 
	{			
		var frames = loadFrames(json);
		var anim:Sequence;
		
		var width = cast(json.size.w, Int);
		var height = cast(json.size.h, Int);
		
		for (frame in frames) {
			anim = animations[frame.animation];
			
			if (anim == null) {
				anim = new Sequence(frame.animation, bitmap, width, height);
				animations[frame.animation] = anim;				
			}			
			anim.add(frame);
		}
	}
	
	/**
	 * Загружаем спрайт-анимацию
	 * @param	path путь к json и png файлу (без расширения)
	 */
	public function load(path: String)
	{		
		var json = resource_manager.getJson(path);	
		validateJson(json);
		
		var imagePath = cast(json.bitmap, String);
		var direcory = Path.directory (path);
		var bitmap = resource_manager.getBitmap(direcory+"/"+imagePath);
		
		loadAnimation(json, bitmap);
	}
	
	public function createSpritesheet():Spritesheet 
	{
		return  new Spritesheet(this);
	}
	
	/**
	 * Ищет максимальный размер у всех анимаций
	 * @return
	 */
	public function getMaxSize():Size
	{
		var width = 0;
		var height = 0;
		for (anim in animations) {
			if (width < anim.width)
				width = anim.width;
			
			if (height < anim.height)
				height = anim.height;
		}
		return new Size(width, height);
	}
}